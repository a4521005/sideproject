from contextlib import contextmanager
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from urllib.request import urlopen
import time
import json
#import pandas as pd

@contextmanager
def create_chrome_driver() -> Chrome:
    driver=Chrome()
    yield driver
    driver.quit()

def go_to_productpage(driver: Chrome):
    url=""
    import time
    urlAll=["https://www.garmin.com.tw/products/wearables/fenix-8-51-amoled-orange/","https://www.garmin.com.tw/products/wearables/quatix-7-pro/","https://www.garmin.com.tw/products/wearables/approach-s70-47-bronze/","https://www.garmin.com.tw/products/wearables/forerunner-165-music-berry/","https://www.garmin.com.tw/products/wearables/descent-mk3i-51-black/"]

    for i in range(0,len(urlAll)):
        driver.get(urlAll[i])
        time.sleep(10)
        cookie_btn_element="truste-consent-button"
        cookie_btn = driver.find_element(By.ID, cookie_btn_element)
        accept= cookie_btn.get_attribute("innerHTML")
        if accept.find("Accept") >= 0:
            accept_button=driver.find_element(
                By.CSS_SELECTOR, cookie_btn_element
            )
            driver.execute_script(
                "arguments[0].click();",
                accept_button
            )
            time.sleep(5)

        addtocart_btn_element = "gotobuy"
        addtocart_btn = driver.find_element(By.ID, addtocart_btn_element)
        html = addtocart_btn.get_attribute("innerHTML")
        if html.find("加入購物車") >= 0:
            url = urlAll[i]
            break
    driver.set_window_size(1500, 2000)

    time.sleep(5)

def click_buy_button(driver: Chrome):
    site_element="div.app__product__info"
    site_element_all= driver.find_element(By.CSS_SELECTOR, site_element)
    html = site_element_all.get_attribute("innerHTML")
    if html.find("gotobuy") >=0:
        if html.find('加入購物車') >= 0:
            buy_button_element="a.btn-addtocart"
            buy_button=driver.find_element(
                By.CSS_SELECTOR, buy_button_element
            )
            # click add to cart加入購物車
            driver.execute_script(
                "arguments[0].click();",
                buy_button
            )
            time.sleep(10)

def shopping_cart_checkout(driver: Chrome):
    shopping_cart_element="div.ec_container"
    shopping_cart_all= driver.find_element(By.CSS_SELECTOR, shopping_cart_element)
    html = shopping_cart_all.get_attribute("innerHTML")
    if html.find("payBillBtn") >= 0:
        checkout_button_element = "payBillBtn"
        checkout_button = driver.find_element(
            By.ID, checkout_button_element
        )
        # click checkout
        driver.execute_script(
            "arguments[0].click();",
            checkout_button
        )
        time.sleep(10)

def login_or_guest_page(driver: Chrome):
    guest_checkout_element = "div.gc__sign-in__box.guestCheckout"
    guest_checkout_all = driver.find_element(By.CSS_SELECTOR, guest_checkout_element)
    html = guest_checkout_all.get_attribute("innerHTML")
    if html.find("submit") >= 0:
        guest_checkout_button_element = "input.guestCheckout__button"
        guest_checkout_button= driver.find_element(
            By.CSS_SELECTOR, guest_checkout_button_element
        )
        # guest_checkout_button訪客結帳
        driver.execute_script(
            "arguments[0].click();",
            guest_checkout_button
        )
        time.sleep(5)

def shipping_page(driver: Chrome):
    #收件人
    name_element = "#shipToAddressInfo > div.new_address_block > div:nth-child(1) > input[type=text]"
    name_input = driver.find_element(By.CSS_SELECTOR, name_element)
    name_input.send_keys("陳嘎米")
    time.sleep(5)
    #聯絡電話
    mobilephone_element = "#shipToAddressInfo > div.new_address_block > div:nth-child(2) > input[type=text]"
    mobilephone_input = driver.find_element(By.CSS_SELECTOR, mobilephone_element)
    mobilephone_input.send_keys("0912345678")
    time.sleep(5)
    #地址
    select_element = driver.find_element(By.NAME, 'shipToCounty')
    select = Select(select_element)
    select.select_by_value('新北市')
    time.sleep(5)
    select_element = driver.find_element(By.NAME, 'shipToDistrict')
    select = Select(select_element)
    select.select_by_value('汐止區')
    time.sleep(5)
    address_element = "#shipToAddressInfo > div.new_address_block > div.item.has_danger > input[type=text]"
    address_input = driver.find_element(By.CSS_SELECTOR, address_element)
    address_input.send_keys("新北市汐止區新台五路一段97號37樓")
    time.sleep(5)
    #email
    email_element = "email"
    email_input = driver.find_element(By.ID, email_element )
    email_input.send_keys("garmin@garmin.com")

    next_btn_element="submitBtn"
    next_btn= driver.find_element(By.ID, next_btn_element)
    # 繼續
    driver.execute_script(
        "arguments[0].click();",
        next_btn
    )
    time.sleep(5)
    # OK
    ok_btn_element="body > div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.no-close.ui-dialog-buttons.ui-draggable.ui-resizable > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button"
    ok_btn = driver.find_element(By.CSS_SELECTOR, ok_btn_element)
    driver.execute_script(
        "arguments[0].click();",
        ok_btn
    )
    # 繼續
    driver.execute_script(
        "arguments[0].click();",
        next_btn
    )
    time.sleep(10)

def payment_page(driver: Chrome):
    #table_subtotal
    table_subtotal_element="td.table_subtotal"
    table_subtotal = driver.find_element(By.CSS_SELECTOR, table_subtotal_element)
    table_subtotal = table_subtotal.get_attribute("innerHTML")
    table_subtotal=table_subtotal.replace("$", "").replace(" ", "").replace(",", "")
    table_subtotal=int(table_subtotal)

    #shipping_costs
    shipping_costs_element="span.highline.shipping_costs"
    shipping_costs= driver.find_element(By.CSS_SELECTOR, shipping_costs_element)
    shipping_costs= shipping_costs.get_attribute("innerHTML")
    shipping_costs=shipping_costs.replace("$", "").replace(" ", "").replace(",", "")
    shipping_costs=int(shipping_costs)

    # totalPrice
    total_price_element="totalPrice"
    total_price= driver.find_element(By.ID, total_price_element)
    total_price= total_price.get_attribute("innerHTML")
    total_price=total_price.replace("$", "").replace(" ", "").replace(",", "")
    total_price=int(total_price)

    total=table_subtotal+shipping_costs
    print("subtotal:",table_subtotal)
    print("shipping_costs:", shipping_costs)
    print("total_price:", total_price)

    print("total_price:", total)
    if total == total_price:
        print("total price is correct, it's pass")
    else:
        print("total price is wrong, it's fail")




if __name__ == '__main__':
    with create_chrome_driver() as chrome_driver:
        go_to_productpage(driver=chrome_driver)
        click_buy_button(driver=chrome_driver)
        shopping_cart_checkout(driver=chrome_driver)
        login_or_guest_page(driver=chrome_driver)
        shipping_page(driver=chrome_driver)
        payment_page(driver=chrome_driver)
        chrome_driver.quit()
