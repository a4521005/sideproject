from contextlib import contextmanager
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import time


@contextmanager
def create_chrome_driver() -> Chrome:
    driver = Chrome()
    yield driver
    driver.quit()



def go_to_findyourwatch_page(driver: Chrome):
    url = "https://www.garmin.com.tw/minisite/find-your-watch/"
    driver.get(url)
    driver.set_window_size(1500, 2000)
    import time
    time.sleep(3)

def click_cookie(driver: Chrome):
    cookie_btn_element="truste-consent-required"
    cookie_btn= driver.find_element(By.ID, cookie_btn_element)
    # click cookie
    driver.execute_script(
        "arguments[0].click();",
        cookie_btn
    )
    time.sleep(3)

def click_recommend_btn(driver: Chrome):
    recommend_btn_element = "#hero > h2 > a"
    recommend_btn = driver.find_element(By.CSS_SELECTOR, recommend_btn_element)
    # click checkout
    driver.execute_script(
        "arguments[0].click();",
        recommend_btn
    )
    time.sleep(5)

#如果生活忙碌 我會找時間運動
def question_one(driver: Chrome):
    question_one_btn_element="#wizard > div > div:nth-child(1) > div > ul > li.f__do__yes > label"
    question_one_btn= driver.find_element(By.CSS_SELECTOR, question_one_btn_element)
    # click 會
    driver.execute_script(
        "arguments[0].click();",
        question_one_btn
    )
    time.sleep(3)
    next_btn_element="btn-next"
    next_btn= driver.find_element(By.ID, next_btn_element)
    # click 下一題
    driver.execute_script(
        "arguments[0].click();",
        next_btn
    )
    time.sleep(5)

#我最常做的運動是…
def question_two(driver: Chrome):
    question_two_btn_element="#wizard > div > div:nth-child(2) > div > ul > li.f__sports__walking > label > p"
    question_two_btn= driver.find_element(By.CSS_SELECTOR, question_two_btn_element)
    # click 步行
    driver.execute_script(
        "arguments[0].click();",
        question_two_btn
    )
    time.sleep(3)
    next_btn_element="btn-next"
    next_btn= driver.find_element(By.ID, next_btn_element)
    # click 下一題
    driver.execute_script(
        "arguments[0].click();",
        next_btn
    )
    time.sleep(5)

#我的預算是...
def question_three(driver: Chrome):
    question_three_btn_element="#wizard > div > div:nth-child(3) > div > ul > li.f__budget__cheapest > label > p"
    question_three_btn= driver.find_element(By.CSS_SELECTOR, question_three_btn_element)
    # click 1萬元以下
    driver.execute_script(
        "arguments[0].click();",
        question_three_btn
    )
    time.sleep(3)
    next_btn_element="btn-next"
    next_btn= driver.find_element(By.ID, next_btn_element)
    # click 下一題
    driver.execute_script(
        "arguments[0].click();",
        next_btn
    )
    time.sleep(5)

#我喜歡智慧手錶有...
def question_four(driver: Chrome):
    question_four_btn_element="#wizard > div > div:nth-child(4) > div > ul > li.f__screen__analog > label > p"
    question_four_btn= driver.find_element(By.CSS_SELECTOR, question_four_btn_element)
    # click 實體指針+數位螢幕
    driver.execute_script(
        "arguments[0].click();",
        question_four_btn
    )
    time.sleep(3)
    next_btn_element="btn-next"
    next_btn= driver.find_element(By.ID, next_btn_element)
    # click 下一題
    driver.execute_script(
        "arguments[0].click();",
        next_btn
    )
    time.sleep(5)

#我的智慧手錶至少要能…
def question_five(driver: Chrome):
    question_five_btn_element="#wizard > div > div:nth-child(7) > div > ul > li.f__life__pulse_ox > label > p"
    question_five_btn= driver.find_element(By.CSS_SELECTOR, question_five_btn_element)
    # click 偵測血氧
    driver.execute_script(
        "arguments[0].click();",
        question_five_btn
    )
    time.sleep(3)
    next_btn_element="btn-next"
    next_btn= driver.find_element(By.ID, next_btn_element)
    # click 看結果
    driver.execute_script(
        "arguments[0].click();",
        next_btn
    )
    time.sleep(10)

if __name__ == '__main__':
    with create_chrome_driver() as chrome_driver:
        go_to_findyourwatch_page(driver=chrome_driver)
        click_cookie(driver=chrome_driver)
        click_recommend_btn(driver=chrome_driver)
        question_one(driver=chrome_driver)
        question_two(driver=chrome_driver)
        question_three(driver=chrome_driver)
        question_four(driver=chrome_driver)
        question_five(driver=chrome_driver)
        chrome_driver.quit()
